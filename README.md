#Update volume.yaml file:

nodeAffinity:
    required:
      nodeSelectorTerms:
      - matchExpressions:
        - key: kubernetes.io/hostname
          operator: In
          values:
          - ip-172-31-52-149.ec2.internal


replace ip-172-31-52-149.ec2.internal with node name in kubectl get nodes:
[root@ip-172-31-52-149 ~]# kubectl get nodes
NAME                            STATUS   ROLES           AGE   VERSION
ip-172-31-50-117.ec2.internal   Ready    <none>          33m   v1.26.0
ip-172-31-52-149.ec2.internal   Ready    control-plane   34m   v1.26.0
ip-172-31-59-12.ec2.internal    Ready    <none>          33m   v1.26.0

#Add label to kubernetes node using command:
kubectl label nodes ip-172-31-52-149.ec2.internal tool:jenkins

#Update deployment.yaml file add nodeSelector:

     volumeMounts:
            - name: jenkins-data
              mountPath: /var/jenkins_home         
      nodeSelector:
        tool: jenkins
      volumes:
        - name: jenkins-data
          persistentVolumeClaim:
              claimName: jenkins-pv-claim

#Deploy Jenkins K8S manifest files in below order using kubectl apply -f <manifest-file> command:
1. namespace.yaml
2. serviceAccount.yaml
3. volume.yaml
4. deployment.yaml
5. service.yaml



